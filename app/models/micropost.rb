class Micropost < ApplicationRecord
  # rails g model Micropost content:text user:references によって作成
  belongs_to :user
  # ラムダ式（無名関数）を使う
  default_scope -> { order(created_at: :desc) }
  mount_uploader :picture, PictureUploader
  validates :user_id, presence: true
  validates :content, presence: true, length: { maximum: 140 }
  validate  :picture_size
  
  private
  
    # アップロードされた画像サイズをバリデーションする
    def picture_size
      if picture.size > 5.megabytes
        errors.add(:picture, "should be less than 5MB")
      end
    end
end
